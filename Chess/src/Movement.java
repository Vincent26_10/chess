
public class Movement {
	public char originColumn;
	public int originRow;
	public char destinationColumn;
	public int destinationRow;
	public Piece p;
	
	public Movement(Piece p,char originColumn, int originRow,  char destinationColumn, int destinationRow) {
		this.originColumn=originColumn;
		this.originRow=originRow;
		this.destinationColumn=destinationColumn;
		this.destinationRow=destinationRow;
		this.p = p;
	}
	
	@Override
	public String toString() {
		 return p+"from " + originColumn+ originRow+"to"+destinationColumn+ destinationRow;
	}

	public char getOriginColumn() {
		return originColumn;
	}

	public int getOriginRow() {
		return originRow;
	}

	public char getDestinationColumn() {
		return destinationColumn;
	}

	public int getDestinationRow() {
		return destinationRow;
	}

	public Piece getP() {
		return p;
	}
	
	

}
