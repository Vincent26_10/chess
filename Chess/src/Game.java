
public class Game {
	private Movement[] moves;
	private Board b;
	int position;

	public Game() {
		moves = new Movement[100];
		b = new Board();
		position = 0;
	}

	public void movePiece(Movement mov) {
		if (b.movePiece(mov)) {
			moves[position] = mov;
			b.movePiece(mov);
			position++;
		}
	}

	@Override
	public String toString() {
		String s = "";
		s += b.toString() + "\n";
		for (int i = 0; i < position; i++) {
			s += moves[i] + "\n";
		}
		return s;
	}
	
	public Piece getPiece(char col,int row) {
		return b.getPieceAt(col, row);
	}
}
